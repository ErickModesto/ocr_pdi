#!/usr/bin/env python3
#
# OCR and TTS: App GUI
# Optical Character Recognition with Speech Synthesis: Graphical User Interface
# of the Application
#
# Copyright (2018) LabVIS - PPGCC - UFPA 
# Interaction, Visualization and Intelligent Systems Lab 
# Computer Science Graduation Program
# Federal University of Para
# Belém, Brazil
#
# Authors: Nov 2018
# Cassio Batista - cassio.batista.13@gmail.com
# Erick Campos   - erick.c.modesto@gmail.com

import os
import sys
sys.path.insert(0, 'src')

# https://stackoverflow.com/questions/29615235/pyttsx-no-module-named-engine
# https://pythonprogramminglanguage.com/text-to-speech/
import cv2
import pyttsx3
import pytesseract

from PyQt5 import QtCore, QtGui, QtWidgets
import threading
import numpy as np
from queue import Queue
import time

import info
from actgui import MainUIActions
from actcv import ImageCVActions

# https://stackoverflow.com/questions/20749819/pyqt5-failing-import-of-qtgui
# https://stackoverflow.com/questions/19877306/nameerror-global-name-unicode-is-not-defined-in-python-3
# https://gis.stackexchange.com/questions/88660/nameerror-name-qimage-is-not-defined

class MainGUI(QtWidgets.QMainWindow):
	def __init__(self, screen_size):
		super(MainGUI, self).__init__()
		# screen dimensions
		self.label_width  = screen_size.width()/100*35
		self.label_height = screen_size.height()/100*55

		self.hline = QtWidgets.QFrame();
		self.hline.setGeometry(QtCore.QRect(320, 150, 118, 3));
		self.hline.setFrameShape(QtWidgets.QFrame.HLine);
		self.hline.setFrameShadow(QtWidgets.QFrame.Sunken);

		# load tts engine (DEPRECTAETED)
		self.tts_engine = None
		self.text = Queue(maxsize=1)

		# load global var
		self.last_dir = info.ROOT_DIR_PATH
		self.image_output = None
		self.image_input  = None

		# FIXME: is this the right way to make a different class handle widgets?
		self.dip = ImageCVActions(self)
		self.act = MainUIActions(self, self.dip)

		# init widgets
		self.init_main_screen()
		self.init_toolbar()

		self.act.checklist.append(self.grey_check)
		self.act.checklist.append(self.gamma_check)
		self.act.checklist.append(self.thresh_adapt_radio)
		self.act.checklist.append(self.thresh_manual_radio)
		self.act.checklist.append(self.median_check)
		self.act.checklist.append(self.invert_check)
		self.act.checklist.append(self.skew_check)

		# disable clickable widgets
		self.act.enable_gui(False)

	def init_main_screen(self):
		# first pic qlabel
		self.in_img_label = QtWidgets.QLabel(u'Input image')
		self.in_img_label.setAlignment(QtCore.Qt.AlignCenter)
		self.in_img_label.setMinimumWidth(self.label_width)
		self.in_img_label.setMinimumHeight(self.label_height)
		self.in_img_label.setStyleSheet("border: 1px solid black")

		# second pic qlabel
		self.out_img_label = QtWidgets.QLabel(u'Output image')
		self.out_img_label.setAlignment(QtCore.Qt.AlignCenter)
		self.out_img_label.setMinimumWidth(self.label_width)
		self.out_img_label.setMinimumHeight(self.label_height)
		self.out_img_label.setStyleSheet("border: 1px solid black")

		# put both figs side by side horizontally
		hbl_imgs = QtWidgets.QHBoxLayout()
		hbl_imgs.addWidget(self.in_img_label)
		hbl_imgs.addSpacing(20)
		hbl_imgs.addWidget(self.out_img_label)

		# store this horizontal merge into a group
		gb_img = QtWidgets.QGroupBox(u'Input Image')
		gb_img.setLayout(hbl_imgs)
		# ---

		# output: machine-editable text widget
		self.out_text = QtWidgets.QPlainTextEdit()
		self.out_text.setReadOnly(True)
		self.out_text.document().setPlainText(
					u'they were crying when their sons left,\n' + \
						'god is wearing black,\n' + \
						'he\'s gone so far, to find the truth,\n' + \
						'he\'s never coming back')

		# tts "speak" button
		self.tts_button = QtWidgets.QToolButton()
		self.tts_button.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
		self.tts_button.setStatusTip(u'Call PyTTSX package')
		self.tts_button.setToolTip  (u'Call PyTTSX package')
		self.tts_button.setIcon(QtGui.QIcon(os.path.join(
					info.SRC_DIR_PATH, 'images', 'speaker.png')))
		self.tts_button.setIconSize(QtCore.QSize(80,80))
		self.tts_button.setText(u'Speak')
		self.tts_button.clicked.connect(self.tts_apply)

		# arrange text and button into a horizontal layout
		hbl_txt = QtWidgets.QHBoxLayout()
		hbl_txt.addWidget(self.out_text)
		hbl_txt.addWidget(self.tts_button)

		# group that layout with itself
		gb_txt = QtWidgets.QGroupBox(u'Output Text')
		gb_txt.setLayout(hbl_txt)

		# merge groups vertically: both pics at the top, text at the bottom
		vbl_io_panel = QtWidgets.QVBoxLayout()
		vbl_io_panel.addWidget(gb_img)
		vbl_io_panel.addWidget(gb_txt)

		# store this merge in a group
		gb_io_panel = QtWidgets.QGroupBox(u'I/O Panel')
		gb_io_panel.setLayout(vbl_io_panel)
		# ---------------------------------------------------------------------

		# grey binary check
		self.grey_check = QtWidgets.QCheckBox(u'Greyscale?')
		self.grey_check.stateChanged.connect(self.act.grey_state_change)

		# FIXME arrange grey check alone in a vertical layout
		vbl_grey = QtWidgets.QVBoxLayout()
		vbl_grey.addWidget(self.grey_check)

		# store layout in a group
		gb_grey = QtWidgets.QGroupBox()
		gb_grey.setLayout(vbl_grey)
		# ----

		# gamma binary check
		self.gamma_check = QtWidgets.QCheckBox(u'Gamma correction?')
		self.gamma_check.stateChanged.connect(self.act.gamma_state_change)

		# gamma value shower
		self.gamma_edit = QtWidgets.QLineEdit()
		self.gamma_edit.setReadOnly(True)
		self.gamma_edit.setText(u'1.00')
		self.gamma_edit.setFixedWidth(40)

		# put label and line edit side by side
		hbl_gamma = QtWidgets.QHBoxLayout()
		hbl_gamma.addWidget(self.gamma_check)
		hbl_gamma.addWidget(self.gamma_edit)

		# create gamma slider
		self.gamma_slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
		self.gamma_slider.setMaximum(201) # FIXME
		self.gamma_slider.valueChanged.connect(self.act.gamma_value_change)

		self.act.gamma_state_change()

		# place line edit and slider one above the other
		vbl_gamma = QtWidgets.QVBoxLayout()
		vbl_gamma.addLayout(hbl_gamma)
		vbl_gamma.addWidget(self.gamma_slider)

		# group everything into a box
		gb_gamma = QtWidgets.QGroupBox()
		gb_gamma.setLayout(vbl_gamma)
		# ---

		# create thresh radio buttons
		self.thresh_adapt_radio  = QtWidgets.QRadioButton(u'Adapt. threshold')
		self.thresh_manual_radio = QtWidgets.QRadioButton(u'Manual threshold')
		self.thresh_adapt_radio.toggled.connect(self.act.thresh_adapt_state_change)
		self.thresh_manual_radio.toggled.connect(self.act.thresh_manual_state_change)

		# group radio buttons: let 'em be tied together
		thresh_bgroup = QtWidgets.QButtonGroup()
		thresh_bgroup.addButton(self.thresh_adapt_radio)
		thresh_bgroup.addButton(self.thresh_manual_radio)

		# thresh adapt value shower
		self.thresh_adapt_edit = QtWidgets.QLineEdit()
		self.thresh_adapt_edit.setReadOnly(True)
		self.thresh_adapt_edit.setText(u'127') # FIXME
		self.thresh_adapt_edit.setFixedWidth(40)

		# put radio-label and line edit side by side
		hbl_thresh_adapt_view = QtWidgets.QHBoxLayout()
		hbl_thresh_adapt_view.addWidget(self.thresh_adapt_radio)
		hbl_thresh_adapt_view.addWidget(self.thresh_adapt_edit)

		# create thresh adapt slider
		self.thresh_adapt_slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
		self.thresh_adapt_slider.setMinimum(0)  # FIXME
		self.thresh_adapt_slider.setMaximum(30) # FIXME
		self.thresh_adapt_slider.setValue(1)    # FIXME
		self.thresh_adapt_slider.valueChanged.connect(self.act.thresh_adapt_value_change)

		# thresh adapt spin box
		self.thresh_adapt_spin = QtWidgets.QSpinBox() # kernel
		self.thresh_adapt_spin.setRange(3, 999)
		self.thresh_adapt_spin.setSingleStep(2)
		self.thresh_adapt_spin.setReadOnly(True)
		self.thresh_adapt_spin.setFixedWidth(45)
		self.thresh_adapt_spin.valueChanged.connect(self.act.thresh_adapt_value_change)

		hbl_thresh_adapt_control = QtWidgets.QHBoxLayout()
		hbl_thresh_adapt_control.addWidget(self.thresh_adapt_spin)
		hbl_thresh_adapt_control.addWidget(self.thresh_adapt_slider)

		# thresh manual value shower
		self.thresh_manual_edit = QtWidgets.QLineEdit()
		self.thresh_manual_edit.setReadOnly(True)
		self.thresh_manual_edit.setText(u'127') # FIXME
		self.thresh_manual_edit.setFixedWidth(40)

		# put label and line edit side by side
		hbl_thresh_manual = QtWidgets.QHBoxLayout()
		hbl_thresh_manual.addWidget(self.thresh_manual_radio)
		hbl_thresh_manual.addWidget(self.thresh_manual_edit)

		# create thresh slider
		self.thresh_manual_slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
		self.thresh_manual_slider.setMaximum(255) # FIXME
		self.thresh_manual_slider.setValue(127) # FIXME
		self.thresh_manual_slider.valueChanged.connect(self.act.thresh_manual_value_change)

		self.act.thresh_adapt_state_change()
		self.act.thresh_manual_state_change()

		# median check box
		self.median_check = QtWidgets.QCheckBox(u'Median blur?')
		self.median_check.stateChanged.connect(self.act.median_state_change)

		# median spin box
		self.median_spin = QtWidgets.QSpinBox()
		self.median_spin.setRange(1, 99)
		self.median_spin.setSingleStep(2)
		self.median_spin.setReadOnly(True)
		self.median_spin.valueChanged.connect(self.act.median_value_change)

		self.act.median_state_change()

		# arrange median checkbox and spinbox into a horizontal layout
		hbl_median = QtWidgets.QHBoxLayout()
		hbl_median.addWidget(self.median_check)
		hbl_median.addWidget(self.median_spin)

		# place thresh horizontal layout, thresh slider and median horizontal
		# layout into a vertical layout, one above the other 
		vbl_binarization = QtWidgets.QVBoxLayout()
		vbl_binarization.addLayout(hbl_thresh_adapt_view)
		vbl_binarization.addLayout(hbl_thresh_adapt_control)
		vbl_binarization.addWidget(self.hline)
		vbl_binarization.addLayout(hbl_thresh_manual)
		vbl_binarization.addWidget(self.thresh_manual_slider)
		vbl_binarization.addLayout(hbl_median)

		# group everything into a box
		gb_binarization = QtWidgets.QGroupBox(u'Binarization')
		gb_binarization.setLayout(vbl_binarization)
		# ---

		# invert check box
		self.invert_check = QtWidgets.QCheckBox(u'Invert Colors?')
		self.invert_check.stateChanged.connect(self.act.invert_state_change)

		# skew check box
		self.skew_check = QtWidgets.QCheckBox(u'Skew correction?')
		self.skew_check.stateChanged.connect(self.act.skew_state_change)

		# skew value shower line edit
		self.skew_edit = QtWidgets.QLineEdit()
		self.skew_edit.setReadOnly(True)
		self.skew_edit.setText(u'0')
		self.skew_edit.setFixedWidth(40)

		# put label and line edit side by side
		hbl_skew = QtWidgets.QHBoxLayout()
		hbl_skew.addWidget(self.skew_check)
		hbl_skew.addWidget(self.skew_edit)

		# create skew slider
		self.skew_slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
		self.skew_slider.setMinimum(-45)
		self.skew_slider.setMaximum(45)
		self.skew_slider.valueChanged.connect(self.act.skew_value_change)

		self.act.skew_state_change()

		# place skew horizontal layout and skew slider one above the other
		vbl_inv_skew = QtWidgets.QVBoxLayout()
		vbl_inv_skew.addWidget(self.invert_check)
		vbl_inv_skew.addLayout(hbl_skew)
		vbl_inv_skew.addWidget(self.skew_slider)

		# group everything into a box
		gb_inv_skew = QtWidgets.QGroupBox()
		gb_inv_skew.setLayout(vbl_inv_skew)
		# ----

		# language combobox
		self.lang_combo = QtWidgets.QComboBox()
		self.lang_combo.addItem(u'')
		self.lang_combo.addItem(u'Brazilian Portuguese')
		self.lang_combo.addItem(u'British English')

		vbl_lang = QtWidgets.QVBoxLayout()
		vbl_lang.addWidget(self.lang_combo)

		gb_lang = QtWidgets.QGroupBox(u'Select language:')
		gb_lang.setLayout(vbl_lang)
		# ----

		# ocr button
		self.ocr_button = QtWidgets.QToolButton()
		self.ocr_button.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
		self.ocr_button.setStatusTip(u'Call OCR package')
		self.ocr_button.setToolTip(u'Call OCR package')
		self.ocr_button.setFixedWidth(226)
		self.ocr_button.setIcon(QtGui.QIcon(
					os.path.join(info.SRC_DIR_PATH, 'images', 'ocr.png')))
		self.ocr_button.setIconSize(QtCore.QSize(2.3*80,80))
		self.ocr_button.setText(u'OCR')
		self.ocr_button.clicked.connect(self.ocr_apply)

		# group all config mini layouts vertically
		vbl_config_panel = QtWidgets.QVBoxLayout()
		vbl_config_panel.setAlignment(QtCore.Qt.AlignCenter)
		vbl_config_panel.addWidget(gb_grey)
		vbl_config_panel.addSpacing(-20)
		vbl_config_panel.addWidget(gb_gamma)
		vbl_config_panel.addWidget(gb_binarization)
		vbl_config_panel.addSpacing(-20)
		vbl_config_panel.addWidget(gb_inv_skew)
		vbl_config_panel.addWidget(gb_lang)
		vbl_config_panel.addWidget(self.ocr_button)

		# put into a group
		gb_config_panel = QtWidgets.QGroupBox(u'Settings Panel')
		gb_config_panel.setLayout(vbl_config_panel)
		gb_config_panel.setFixedWidth(250)

		# main ui
		self.vb_layout_main = QtWidgets.QHBoxLayout()
		self.vb_layout_main.addWidget(gb_io_panel)
		self.vb_layout_main.addWidget(gb_config_panel)

		# exec main ui
		wg_central = QtWidgets.QWidget()
		wg_central.setLayout(self.vb_layout_main)
		self.setCentralWidget(wg_central)

	def init_toolbar(self):
		act_exit = QtWidgets.QAction(QtGui.QIcon(os.path.join(
					info.SRC_DIR_PATH, 'images', 'x.png')), u'&Quit', self)
		act_exit.setShortcut('Ctrl+Q')
		act_exit.setStatusTip(u'Close OCR app')
		act_exit.triggered.connect(self.close)

		act_open = QtWidgets.QAction(QtGui.QIcon(os.path.join(
					info.SRC_DIR_PATH, 'images', 'open.png')), u'&Open', self)
		act_open.setShortcut('Ctrl+O')
		act_open.setStatusTip(u'Open an image')
		act_open.triggered.connect(self.open_img)

		act_about = QtWidgets.QAction(QtGui.QIcon(os.path.join(
					info.SRC_DIR_PATH, 'images', 'about.png')), u'&About', self)
		act_about.setShortcut('Ctrl+I')
		act_about.setStatusTip(u'About OCR app')
		act_about.triggered.connect(self.about)

		act_clean = QtWidgets.QAction(QtGui.QIcon(os.path.join(
					info.SRC_DIR_PATH, 'images', 'clean.png')), u'&Clear', self)
		act_clean.setStatusTip(u'Reset OCR GUI to defaults')
		act_clean.triggered.connect(self.clear)

		self.statusBar()
	
		toolbar = self.addToolBar('Standard')
		toolbar.addAction(act_exit)
		toolbar.addAction(act_open)
		toolbar.addAction(act_about)
		toolbar.addAction(act_clean)

	def quit_app(self):
		QtWidgets.qApp.quit() # FIXME
		reply = QtWidgets.QMessageBox.question(self, u'Fechar OCR App', 
					u'Do you really want to quit?\n',
					QtWidgets.QMessageBox.No | QtWidgets.QMessageBox.Yes)
		if reply == QtWidgets.QMessageBox.Yes:
			QtWidgets.qApp.quit()
		else:
			return

	def open_img(self):
		filename = QtWidgets.QFileDialog.getOpenFileName(self,
				u'Open image file', 
				self.last_dir, u'Image Files (*.jpg *.jpeg *.png)')
		filename = str(filename[0])
		if filename is u'':
			return

		# record last dir you retrieved a file from
		self.last_dir = os.path.dirname(filename)

		# load input image
		self.image_input = cv2.imread(filename)

		# resize image to fit widget
		# NOTE: at start, output is the same as the input, foda-se
		self.image_output = self.dip.cv_fitresize(self.image_input)

		# display images over qlabel widgets
		self.act.display_img(self.dip.image_resized, self.in_img_label)
		self.act.display_img(self.image_output, self.out_img_label)

		self.grey_check.setEnabled(True)

	def about(self):
		QtWidgets.QMessageBox.information(self, u'About', info.INFO)
		return

	def clear(self):
		self.init_main_screen()

	def tts_speak(self, text):
		if self.tts_engine is not None and self.tts_engine.isBusy():
			self.tts_engine.stop()
		else:
			self.tts_engine = pyttsx3.init()
			if self.lang_combo.currentIndex() == 1:
				language = 'brazil'
			elif self.lang_combo.currentIndex() == 2:
				language = 'english-uk'
			self.tts_engine.setProperty('voice', language)
			self.tts_engine.say(text)
			self.tts_engine.runAndWait()

	def tts_apply(self):
		text = self.out_text.document().toPlainText()
		text = ''.join(text.split('-\n'))
		t = threading.Thread(target=self.tts_speak, args=(text,))
		t.start()

	def ocr_recog(self, language):
		text = pytesseract.image_to_string(self.image_output, lang=language)
		self.text.put(text)

	def ocr_apply(self):
		if not all([self.skew_check.isChecked(), self.lang_combo.currentIndex()>0]):
			print('not ok')
			return

		if self.lang_combo.currentIndex() == 1:
			language = 'por'
		elif self.lang_combo.currentIndex() == 2:
			language = 'eng'

		t = threading.Thread(target=self.ocr_recog, args=(language,)) 
		t.start()

		while self.text.empty():
			## TODO I need a slot right here to update text editor
			#self.out_text.moveCursor(QtGui.QTextCursor.End)
			#self.out_text.document().setPlainText(' .')
			time.sleep(0.5)

		self.out_text.document().clear()
		self.out_text.document().setPlainText(self.text.get())


if __name__=='__main__':
	app = QtWidgets.QApplication(sys.argv)
	screen = app.primaryScreen()
	window = MainGUI(screen.size())
	window.setWindowTitle(info.TITLE)
	window.show() if screen.size().height() > 800 else window.showMaximized()
	sys.exit(app.exec_())
### EOF ###
