#!/usr/bin/env python3
#
# OCR and TTS: App GUI
# Optical Character Recognition with Speech Synthesis: Graphical User Interface
# of the Application
#
# Copyright (2018) LabVIS - PPGCC - UFPA
# Interaction, Visualization and Intelligent Systems Lab 
# Computer Science Graduation Program
# Federal University of Para
# Belém, Brazil
#
# Authors: Nov 2018
# Cassio Batista - cassio.batista.13@gmail.com
# Erick Campos   - erick.c.modesto@gmail.com

from PyQt5 import QtCore, QtGui
import info
from numpy import pi

class MainUIActions:
	def __init__(self, main_gui, imgs_gui):
		super(MainUIActions, self).__init__()
		self.TAG = 'ACT_GUI: '
		self.gui = main_gui
		self.dip = imgs_gui
		self.checklist = []
		self.imglist = [ \
			self.dip.image_greyed,
			self.dip.image_gammaed,
			self.dip.image_binarized,
			self.dip.image_blurred,
			self.dip.image_inverted,
			self.dip.image_skewed \
		]

	def display_img(self, img, widget):
		if img is None:
			return
		qformat = QtGui.QImage.Format_Indexed8
		if len(img.shape) == 3:
			if img.shape[2] == 4:
				qformat = QtGui.QImage.Format_RGBA8888
			else:
				qformat = QtGui.QImage.Format_RGB888
		img = QtGui.QImage(img, img.shape[1], img.shape[0], img.strides[0], qformat)
		img.rgbSwapped()

		widget.setPixmap(QtGui.QPixmap.fromImage(img))
		widget.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)

	# grey check box
	def grey_state_change(self):
		if self.gui.grey_check.isChecked():
			self.gui.image_output = self.dip.cv_rgb2grey()
			self.checklist[1].setEnabled(True)
		else:
			self.gui.image_output = self.dip.cv_grey2rgb()
			for i in range(1, len(self.checklist)):
				self.checklist[i].setChecked(False)
				self.checklist[i].setEnabled(False)
				if i < len(self.imglist):
					self.imglist[i] = None
		self.display_img(self.gui.image_output, self.gui.out_img_label)

	# gamma check box
	def gamma_state_change(self):
		self.gui.gamma_edit.setEnabled(self.gui.gamma_check.isChecked())
		self.gui.gamma_slider.setEnabled(self.gui.gamma_check.isChecked())
		if self.gui.gamma_check.isChecked():
			self.gui.gamma_slider.setStyleSheet('')
			self.gamma_value_change() # FIXME
			self.checklist[2].setEnabled(True)
			self.checklist[3].setEnabled(True)
		else:
			self.gui.gamma_slider.setStyleSheet(info.QSLIDER_DISABLE)
			self.gui.gamma_slider.setValue(100)
			for i in range(2, len(self.checklist)):
				self.checklist[i].setChecked(False)
				self.checklist[i].setEnabled(False)
				if i < len(self.imglist):
					self.imglist[i] = None
			self.gui.image_output = self.dip.cv_ungammacorrect()
			self.display_img(self.gui.image_output, self.gui.out_img_label)

	# gamma slider
	# https://docs.opencv.org/3.4/d3/dc1/tutorial_basic_linear_transform.html
	# https://stats.stackexchange.com/questions/70801/how-to-normalize-data-to-0-1-range
	def gamma_value_change(self):
		def min_max_norm(v, oldmin, oldmax, newmin, newmax):
			if v > 200:
				v = 200
			return (newmax-newmin)/(oldmax-oldmin)*(v-oldmax)+newmax
		gamma = self.gui.gamma_slider.value()
		if gamma < 100:
			gamma = min_max_norm(gamma, 0, 100, 0.1, 1.0)
		elif gamma > 100:
			gamma = min_max_norm(gamma, 100, 200, 1.0, 10.0)
		else:
			gamma /= 100
		self.gui.gamma_edit.setText('%.2f' % float(gamma))
		if self.dip.image_resized is None:
			return
		self.gui.image_output = self.dip.cv_gammacorrect(gamma)
		self.display_img(self.gui.image_output, self.gui.out_img_label)

	# thresh adapt radio button
	def thresh_adapt_state_change(self):
		self.gui.thresh_adapt_edit.setEnabled(self.gui.thresh_adapt_radio.isChecked())
		self.gui.thresh_adapt_spin.setEnabled(self.gui.thresh_adapt_radio.isChecked())
		self.gui.thresh_adapt_slider.setEnabled(self.gui.thresh_adapt_radio.isChecked())
		self.gui.thresh_adapt_spin.setReadOnly(not self.gui.thresh_adapt_radio.isChecked())
		if self.gui.thresh_adapt_radio.isChecked():
			self.checklist[4].setEnabled(True)
			self.gui.thresh_adapt_slider.setStyleSheet('')
			self.thresh_adapt_value_change()
		else:
			self.gui.thresh_adapt_slider.setStyleSheet(info.QSLIDER_DISABLE)
			for i in range(3, len(self.imglist)):
				self.imglist[i] = None
			self.gui.image_output = self.dip.cv_unthresh()
		self.display_img(self.gui.image_output, self.gui.out_img_label)

	# thresh slider
	def thresh_adapt_value_change(self):
		bsize = self.gui.thresh_adapt_spin.value() # kernel
		const = self.gui.thresh_adapt_slider.value()
		self.gui.thresh_adapt_edit.setText(str(const))
		if self.dip.image_resized is None:
			return
		self.gui.image_output = self.dip.cv_adapt_thresh(bsize, const)
		self.display_img(self.gui.image_output, self.gui.out_img_label)

	# thresh adapt radio button
	def thresh_manual_state_change(self):
		self.gui.thresh_manual_edit.setEnabled(self.gui.thresh_manual_radio.isChecked())
		self.gui.thresh_manual_slider.setEnabled(self.gui.thresh_manual_radio.isChecked())
		if self.gui.thresh_manual_radio.isChecked():
			self.checklist[4].setEnabled(True)
			self.gui.thresh_manual_slider.setStyleSheet('')
			self.thresh_manual_value_change()
		else:
			self.gui.thresh_manual_slider.setStyleSheet(info.QSLIDER_DISABLE)
			for i in range(3, len(self.imglist)):
				self.imglist[i] = None
			self.gui.image_output = self.dip.cv_unthresh()
			self.display_img(self.gui.image_output, self.gui.out_img_label)

	# thresh slider
	def thresh_manual_value_change(self):
		thresh = self.gui.thresh_manual_slider.value()
		self.gui.thresh_manual_edit.setText(str(thresh))
		if self.dip.image_resized is None:
			return
		self.gui.image_output = self.dip.cv_manual_thresh(thresh)
		self.display_img(self.gui.image_output, self.gui.out_img_label)

	# median check box
	def median_state_change(self):
		self.gui.median_spin.setEnabled(self.gui.median_check.isChecked())
		self.gui.median_spin.setReadOnly(not self.gui.median_check.isChecked())
		if self.gui.median_check.isChecked():
			self.median_value_change() # FIXME
			self.checklist[5].setEnabled(True)
		else:
			self.gui.image_output = self.dip.cv_unblur()
			self.gui.median_spin.setValue(1)
			for i in range(5, len(self.checklist)):
				self.checklist[i].setChecked(False)
				self.checklist[i].setEnabled(False)
				self.imglist[i] = None
			self.display_img(self.gui.image_output, self.gui.out_img_label)

	# median slider
	def median_value_change(self):
		if self.dip.image_resized is None:
			return
		ksize = self.gui.median_spin.value()
		self.gui.image_output = self.dip.cv_blur(ksize)
		self.display_img(self.gui.image_output, self.gui.out_img_label)

	# invert check box
	def invert_state_change(self):
		if self.gui.invert_check.isChecked():
			self.gui.image_output = self.dip.cv_invertfb()
			self.checklist[6].setEnabled(True)
		else:
			self.gui.image_output = self.dip.cv_revertfb()
			for i in range(6, len(self.checklist)):
				self.checklist[i].setChecked(False)
				self.checklist[i].setEnabled(False)
				self.imglist[i] = None
		self.display_img(self.gui.image_output, self.gui.out_img_label)

	# skew check box
	def skew_state_change(self):
		self.gui.skew_edit.setEnabled(self.gui.skew_check.isChecked())
		self.gui.skew_slider.setEnabled(self.gui.skew_check.isChecked())
		if self.gui.skew_check.isChecked():
			self.gui.skew_slider.setStyleSheet('')
			self.skew_value_change()
		else:
			self.gui.skew_slider.setValue(0)
			self.gui.skew_slider.setStyleSheet(info.QSLIDER_DISABLE)
			self.gui.image_output = self.dip.cv_unskewcorrect()
			self.display_img(self.gui.image_output, self.gui.out_img_label)

	# skew slider
	def skew_value_change(self):
		angle = self.gui.skew_slider.value() 
		self.gui.skew_edit.setText(str(angle) + u'°')
		if self.dip.image_resized is None:
			return
		self.gui.image_output = self.dip.cv_skewcorrect(angle)
		self.display_img(self.gui.image_output, self.gui.out_img_label)

	def enable_gui(self, state):
		self.gui.grey_check.setEnabled(state)
		self.gui.gamma_check.setEnabled(state)
		self.gui.thresh_adapt_radio.setEnabled(state)
		self.gui.thresh_manual_radio.setEnabled(state)
		self.gui.median_check.setEnabled(state)
		self.gui.invert_check.setEnabled(state)
		self.gui.skew_check.setEnabled(state)
### EOF ###
