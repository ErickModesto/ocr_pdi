#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 
#
# UFPA Speech Recorder
#
# Copyright 2017:
# Grupo Falabrasil
# Programa de Pós-Graduação em Ciência da Computação
# Universidade Federal do Pará
#
# Author Nov 2018:
# Cassio Trindade Batista  - cassio.batista.13@gmail.com
#
# Last edited on November, 2018


import sys
import os
import platform
import logging

DEBUG = False

SYS_OS = platform.system().lower()
SYS_HOME_PATH = 'HOMEPATH' if SYS_OS == 'windows' else 'HOME'

SRC_DIR_PATH  = str(os.path.abspath(os.path.dirname( os.path.realpath(__file__))))
ROOT_DIR_PATH = str(os.path.abspath(os.path.join(SRC_DIR_PATH, os.pardir)))

LOGFILE = os.path.join(ROOT_DIR_PATH, 'ufpalog.log')

MAIL = {'cassio':u'cassio.batista.13@gmail.com',
		'erick': u'erick.c.modesto@gmail.com'}

TITLE = u'Optical Character Recognition Application with Speech Synthesis'

INFO =  TITLE + '<br>' \
		u'<br>' + \
		u'Authors:' + \
		u'<br>' + \
		u'Cassio Trindade Batista' + \
		u'<br>' + \
		u'Erick Modesto Campos' + \
		u'<br><br>' + \
		u'Contact:' + \
		u'<br>' + \
		u'<a href=mailto:{0}>{0}</a>'.format(MAIL['cassio']) + \
		u'<br>' + \
		u'<a href=mailto:{0}>{0}</a>'.format(MAIL['erick']) + \
		u'<br><br>' + \
		u'Copyleft 2018' + \
		u'<br>' + \
		u'Visualization, Interaction and Intelligent Systems Lab' + \
		u'<br>' + \
		u'Institute of Exact and Natural Sciences' + \
		u'<br>' + \
		u'Federal University of Pará' + \
		u'<br>' + \
		u'Belém, Brazil'

# http://doc.qt.io/archives/qt-4.8/stylesheet-examples.html#customizing-qslider
# https://stackoverflow.com/questions/47691972/qslider-increase-handle-size
QSLIDER_DISABLE = 'QSlider::handle:horizontal {' + \
		'background-color: #c4c4c4;' + \
		'border: 1px solid #acacac;' + \
		'height: 100px;' + \
		'width:  100px;' + \
		'margin: -20px -20px;' + \
		'border-radius: 3px;' + \
	'}'

def img_path(img):
	return os.path.join(SRC_DIR_PATH, 'images', img)

# http://stackoverflow.com/questions/11232230/logging-to-two-files-with-different-settings
def get_logger():
	logging.disable(logging.NOTSET)
	formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

	handler = logging.FileHandler(LOGFILE)
	handler.setFormatter(formatter)

	logger = logging.getLogger('UFPA Logger')
	logger.addHandler(handler)
	logger.setLevel(logging.DEBUG)

	return logger

### EOF ###
