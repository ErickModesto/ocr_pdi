#!/usr/bin/env python3
#
# OCR and TTS: App GUI
# Optical Character Recognition with Speech Synthesis: Graphical User Interface
# of the Application
#
# Copyright (2018) LabVIS - PPGCC - UFPA
# Interaction, Visualization and Intelligent Systems Lab 
# Computer Science Graduation Program
# Federal University of Para
# Belém, Brazil
#
# Authors: Nov 2018
# Cassio Batista - cassio.batista.13@gmail.com
# Erick Campos   - erick.c.modesto@gmail.com

import cv2
import numpy as np

class ImageCVActions:
	def __init__(self, main_gui):
		super(ImageCVActions, self).__init__()
		self.TAG = 'ACT_CV: '
		self.gui = main_gui
		self.image_original  = None
		self.image_resized   = None
		self.image_greyed    = None
		self.image_gammaed   = None
		self.image_binarized = None
		self.image_blurred   = None
		self.image_inverted  = None
		self.image_skewed    = None

	def cv_fitresize(self, img):
		if img is None:
			print(self.TAG + 'error at ' + self.cv_fitresize.__name__ + '()')
			return

		# save image object
		self.image_original = img.copy()

		# get image dimensions from numpy array
		img_height, img_width, img_channels = self.image_original.shape

		# calculate vertical (y) and horizontal (x) proportions between image
		# and qlabel widget
		dy = img_height / self.gui.label_height
		dx = img_width  / self.gui.label_width

		# img bigger  at height: shrink  on y (vertical)
		# img smaller at height: stretch on y (vertical)
		if (dy > 1.05 and dy > dx) or (dy < 0.95 and dy > dx):
			factor = 1/dy
		# img bigger  at width: shrink  on x (horizontal)
		# img smaller at width: stretch on x (horizontal)
		elif (dx > 1.05 and dx > dy) or (dx < 0.95 and dx > dy):
			factor = 1/dx
		# don't rescale at all
		else:
			factor = 1
		
		# resize!
		self.image_resized = cv2.resize(self.image_original, 
					None, fx=factor, fy=factor, interpolation=cv2.INTER_CUBIC)

		return self.image_resized.copy()

	def cv_rgb2grey(self):
		if self.image_resized is None:
			print(self.TAG + 'error at ' + self.cv_rgb2grey.__name__ + '()')
			return
		self.image_greyed = cv2.cvtColor(self.image_resized, cv2.COLOR_RGB2GRAY)
		return self.image_greyed.copy()

	def cv_grey2rgb(self):
		if self.image_resized is None:
			print(self.TAG + 'error at ' + cv_grey2rgb.__name__)
			return
		return self.image_resized.copy()

	def cv_gammacorrect(self, gamma_val):
		if self.image_greyed is None:
			print(self.TAG + 'error at ' + self.cv_gammacorrect.__name__)
			return
		lut = np.empty((1,256), np.uint8)
		for i in range(256):
			lut[0,i] = np.clip(pow(i/255.0, gamma_val)*255.0, 0, 255)
		self.image_gammaed = cv2.LUT(self.image_greyed, lut)
		return self.image_gammaed.copy()

	def cv_ungammacorrect(self):
		if self.image_greyed is None:
			print(self.TAG + 'error at ' + self.cv_ungammacorrect.__name__ + '()')
			return
		return self.image_greyed.copy()

	def cv_adapt_thresh(self, block_size, const_sub):
		if self.image_gammaed is None:
			print(self.TAG + 'error at ' + self.cv_adapt_thresh.__name__ + '()')
			return
		self.image_binarized = cv2.adaptiveThreshold(self.image_gammaed,
					255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, 
					cv2.THRESH_BINARY, block_size, const_sub)
		return self.image_binarized.copy()

	def cv_manual_thresh(self, thresh_val):
		if self.image_gammaed is None:
			print(self.TAG + 'error at ' + self.cv_manual_thresh.__name__ + '()')
			return
		self.image_binarized = cv2.threshold(self.image_gammaed,
					thresh_val, 255, cv2.THRESH_BINARY)[1]
		return self.image_binarized.copy()

	def cv_unthresh(self):
		if self.image_gammaed is None:
			print(self.TAG + 'error at ' + self.cv_unthresh.__name__ + '()')
			return
		return self.image_gammaed.copy()

	def cv_blur(self, kernel_size):
		if self.image_binarized is None:
			print(self.TAG + 'error at ' + self.cv_blur.__name__ + '()')
			return
		self.image_blurred = cv2.medianBlur(self.image_binarized, kernel_size)
		return self.image_blurred.copy()

	def cv_unblur(self):
		if self.image_binarized is None:
			print(self.TAG + 'error at ' + self.cv_unblur.__name__ + '()')
			return
		return self.image_binarized.copy()

	def cv_invertfb(self):
		if self.image_blurred is None:
			print(self.TAG + 'error at ' + self.cv_invertfb.__name__ + '()')
			return
		self.image_inverted = cv2.bitwise_not(self.image_blurred)
		return self.image_inverted.copy()

	def cv_revertfb(self):
		if self.image_blurred is None:
			print(self.TAG + 'error at ' + self.cv_revertfb.__name__  + '()')
			return
		return self.image_blurred.copy()

	# https://gist.github.com/lnlonSA/d38f436552102a3fdf2a306f84ef1299
	def cv_skewcorrect(self, angle_val):
		if self.image_inverted is None:
			print(self.TAG + 'error at ' + self.cv_skewcorrect.__name__ + '()')
			return

		# compute the minimum bounding box:
		non_zero_pixels = cv2.findNonZero(self.image_inverted)
		center, wh, theta = cv2.minAreaRect(non_zero_pixels)

		# TODO what does it do?
		root_mat = cv2.getRotationMatrix2D(center, angle_val, 1)
		rows, cols = self.image_inverted.shape
		image_rotated = cv2.warpAffine(self.image_inverted,
					root_mat, (cols,rows), flags=cv2.INTER_CUBIC)

		# border removing:
		size_x = np.int0(wh[0])
		size_y = np.int0(wh[1])
		if theta > -45:
			temp   = size_x
			size_x = size_y
			size_y = temp

		self.image_skewed = cv2.getRectSubPix(
					image_rotated, (size_y, size_x), center)

		return self.image_skewed.copy()

	def cv_unskewcorrect(self):
		if self.image_inverted is None:
			print(self.TAG + 'error at ' + self.cv_unskewcorrect.__name__ + '()')
			return
		return self.image_inverted.copy()
### EOF ###
